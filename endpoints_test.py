import unittest
import datetime, base64, tempfile, os, json
import datetime
import app
from app import flask

SERVICE_KEY = '4f081f27-8247-4f42-94dc-2097f8404f4d'
PASSWORD = 'n0t1fy4dm1n'
KEY = (SERVICE_KEY + ':' + PASSWORD).encode('utf-8')
AUTHORIZATION = 'Basic ' + str(base64.b64encode(KEY).decode('utf-8'))

# Test data
request_headers = { 'Authorization': AUTHORIZATION, 'Content-Type': 'application/json'}
delete_header =	 { 'Authorization': AUTHORIZATION }
newNotification = json.dumps({ 'name': 'TestTitle', 'content': 'TestContent', 'author': 'TestAuthor'})
setNotification = newNotification

newGroup = json.dumps({'name': 'TestTitle'})
setGroup = newGroup

newUser = json.dumps({'username': 'TestUsername', 'password': 'TestPassword', 'email': 'TestEmail', 'type': 'admin'})
setUser = newUser


class TestEndpoints(unittest.TestCase): 
    def setUp(self):
        app.settings['MODE'] = 'TESTING'
        flask.config['TESTING'] = True
        flask.testing = True
        app.init()

        app.db.Notification.drop_collection()
        app.db.Group.drop_collection()
        app.db.ClientUser.drop_collection()

        self.test_app = flask.test_client()

        if len(app.db.User(service_key=SERVICE_KEY)) == 0:
            user = app.db.User()
            user.username = 'testuser'
            user.password = PASSWORD
            user.service_key = SERVICE_KEY
            user.email = 'test@test.com'
            user.save()

    @classmethod
    def tearDownClass(cls):
        app.db.Notification.drop_collection()
        app.db.Group.drop_collection()
        app.db.ClientUser.drop_collection()

    def test_all_unauthorized(self):
        response = self.test_app.get('/all')
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_all(self):
        response = self.test_app.get('/all', headers=request_headers)
        self.assertEquals(response.status, '200 OK')
        self.is_json(response)

    def test_add_unauthorized(self):
        response = self.test_app.post('/add', data=json.dumps({'name': '', 'content': '', 'author': ''}))
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_add(self):
        response = self.test_app.post('/add', data=newNotification, headers=request_headers)
        self.assertEquals(response.status, '201 CREATED')
        self.is_json(response)

    def test_remove_unauthorized(self):
        id = self.add_notification()

        # Remove last added
        response = self.test_app.delete('/remove/' + id)
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_remove(self):
        id = self.add_notification()

        response = self.test_app.delete('/remove/' + id, headers=request_headers)
        self.assertEquals(response.status, '200 OK')
        self.is_json(response)

    def test_group_get_all_unauthorized(self):
        response = self.test_app.get('/group/all')
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_group_get_all(self):
        response = self.test_app.get('/group/all', headers=request_headers)
        self.assertEquals(response.status, '200 OK')
        self.is_json(response)

    def test_group_add_unauthorized(self):
        response = self.test_app.post('/group/add', data=json.dumps({'name': ''}))
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_group_add(self):
        response = self.test_app.post('/group/add', data=newGroup, headers=request_headers)
        self.assertEquals(response.status, '201 CREATED')
        self.is_json(response)

    def test_group_remove_unauthorized(self):
        _id = self.add_group()

        response = self.test_app.delete('/group/remove/' + _id)
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_group_remove(self):
        _id = self.add_group()

        response = self.test_app.delete('/group/remove/' + _id, headers=request_headers)
        self.assertEquals(response.status, '200 OK')
        self.is_json(response)
        
    def test_user_get_all_unauthorized(self):
        response = self.test_app.get('/user/all')
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_user_get_all(self):
        response = self.test_app.get('/user/all', headers=request_headers)
        self.assertEquals(response.status, '200 OK')
        self.is_json(response)

    def test_user_add_unauthorized(self):
        response = self.test_app.post('/user/add', data=json.dumps({'username': '', 'password': '', 'email': '', 'type': '', 'owned_groups': [], 'subscription_groups': [], 'creationDate': ''}))
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_user_add(self):
        response = self.test_app.post('/user/add', data=newUser, headers=request_headers)
        self.assertEquals(response.status, '201 CREATED')
        self.is_json(response)

    def test_user_groups(self):
        user_id = self.add_user()
        group_id = self.add_group()
        self.test_app.post('user/groups', data=json.dumps({"user_id": user_id, "group_id": group_id}), headers=request_headers)

        response = self.test_app.get('/user/groups/' + user_id, headers=request_headers)
        self.assertEqual(response.status, '200 OK')
        self.is_json(response)

    def test_user_groups_unauthorized(self):
        user_id, group_id = self.subscribe_user()

        response = self.test_app.get('/user/groups/' + user_id)
        self.assertEqual(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_group_unsubscribe_unauthorized(self):
        user_id, group_id = self.subscribe_user()

        response = self.test_app.post('/user/subscriptions/remove', data=json.dumps({'user_id': user_id, 'group_id': group_id}))
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_group_unsubscribe(self):
        user_id, group_id = self.subscribe_user()

        response = self.test_app.post('/user/subscriptions/remove', data=json.dumps({'user_id': user_id, 'group_id': group_id}), headers=request_headers)
        self.assertEquals(response.status, '200 OK')
        self.is_json(response)

    def test_range_unauthorized(self):
        start = str(datetime.date.today() + datetime.timedelta(days=-2))
        end = str(datetime.date.today())
        response = self.test_app.post('/range', data=json.dumps({'start': start, 'end': end}))
        self.assertEquals(response.status, '401 UNAUTHORIZED')
        self.is_json(response)

    def test_range(self):
        start = str(datetime.date.today() + datetime.timedelta(days=-2))
        end = str(datetime.date.today())
        response = self.test_app.post('/range', headers=request_headers, data=json.dumps({'start': start, 'end': end}))
        self.assertEquals(response.status, '200 OK')
        self.is_json(response)

    def subscribe_user(self):
        user_id = self.add_user()
        group_id = self.add_group()
        response = self.test_app.post('user/subscription', data=json.dumps({"user_id": user_id, "group_id": group_id}), headers=request_headers)
        return user_id, group_id

    def add_group(self):
        self.test_app.post('/group/add', data=newGroup, headers=request_headers)
        json_data = self.test_app.get('/group/all', headers = request_headers).data
        id = str(json.loads(json_data.decode('utf-8'))[0]['id'])
        return id

    def add_user(self):
        self.test_app.post('/user/add', data=newUser, headers=request_headers)
        json_data = self.test_app.get('/user/all', headers = request_headers).data
        id = str(json.loads(json_data.decode('utf-8'))[0]['id'])
        return id

    def add_notification(self):
        self.test_app.post('/add', data=newNotification, headers=request_headers)
        json_data = self.test_app.get('/all', headers=request_headers).data
        id = str(json.loads(json_data.decode('utf-8'))[0]['id'])
        return id

    def is_json(self, response_):
        self.assertEquals(response_.mimetype, 'application/json')

if __name__ == '__main__':
    unittest.main()
