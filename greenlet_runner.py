from gevent.wsgi import WSGIServer
import app
import sys

valid_modes = set(['DEBUG', 'TESTING', 'RELEASE'])


def main():
    if len(sys.argv) > 1:
        mode = sys.argv[1]
        if mode not in valid_modes:
            print('Valid run mode options for server are: DEBUG, TESTING, RELEASE')
            return -2
    else:
        print('You must specify run mode, options for server are: DEBUG, TESTING, RELEASE')
        return -1

    app.settings['MODE'] = mode
    app.init()
    http_server = WSGIServer(('', 5000), app.get_flask())
    http_server.serve_forever()


if __name__ == '__main__':
    main()
