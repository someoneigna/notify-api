import unittest
import datetime, base64, tempfile, os, json
import datetime
import app
from app import flask

SERVICE_KEY = '4f081f27-8247-4f42-94dc-2097f8404f4d'
PASSWORD = 'n0t1fy4dm1n'
KEY = (SERVICE_KEY + ':' + PASSWORD).encode('utf-8')
AUTHORIZATION = 'Basic ' + str(base64.b64encode(KEY).decode('utf-8'))

# Test data
request_headers = { 'Authorization': AUTHORIZATION, 'Content-Type': 'application/json'}
delete_header =	 { 'Authorization': AUTHORIZATION }
newNotification = json.dumps({ 'name': 'TestTitle', 'content': 'TestContent', 'author': 'TestAuthor'})
setNotification = newNotification

newGroup = json.dumps({'name': 'TestTitle'})
setGroup = newGroup

newUser = json.dumps({'username': 'TestUsername', 'password': 'TestPassword', 'email': 'TestEmail', 'type': 'admin'})
setUser = newUser


class NotificationTests(unittest.TestCase):
    def setUp(self):
        app.settings['MODE'] = 'TESTING'
        flask.config['TESTING'] = True
        flask.testing = True
        app.init()

        app.db.Notification.drop_collection()
        app.db.Group.drop_collection()
        app.db.ClientUser.drop_collection()

        self.test_app = flask.test_client()

        if len(app.db.User.objects(service_key=SERVICE_KEY)) == 0:
            user = app.db.User()
            user.username = 'testuser'
            user.password = PASSWORD
            user.service_key = SERVICE_KEY
            user.email = 'test@test.com'
            user.save()

    @classmethod
    def tearDownClass(cls):
        app.db.Notification.drop_collection()
        app.db.Group.drop_collection()
        app.db.ClientUser.drop_collection()

    def test_add_onParameter_fails(self):
        value = json.dumps({'name': '', 'content': 'Test content', 'author': 'TEST'})
        response = self.test_app.post('/add', data=value, headers=request_headers)

        self.is_json(response)
        self.assertIn(b'Name parameter is empty', response.data, 'Name should not be empty')

        value = json.dumps({'name': 'Test name', 'content': '', 'author': 'TEST'})
        response = self.test_app.post('/add', data=value, headers=request_headers)

        self.is_json(response)
        self.assertIn(b'Content parameter is empty', response.data, 'Content should not be empty')

        value = json.dumps({'name': 'Test name', 'content': 'Test content', 'author': ''})
        response = self.test_app.post('/add', data=value, headers=request_headers)

        self.is_json(response)
        self.assertIn(b'Author parameter is empty', response.data, 'Author should not be empty')

    def test_set_onEmptyParameter_fails(self):
        id = self.add_notification()
        value = json.dumps({'name': '', 'content': 'Test content', 'author': 'Test author', 'id': id})
        response = self.test_app.post('/set', data=value, headers=request_headers)

        self.is_json(response)
        self.assertIn(b'Name parameter is empty', response.data, 'Name should not be empty')

        value = json.dumps({'name': 'Test name', 'content': '', 'author': 'Test author', 'id': id})
        response = self.test_app.post('/set', data=value, headers=request_headers)

        self.is_json(response)
        self.assertIn(b'Content parameter is empty', response.data, 'Content should not be empty')

        value = json.dumps({'name': 'Test name', 'content': 'Test content', 'author': '', 'id': id})
        response = self.test_app.post('/set', data=value, headers=request_headers)

        self.is_json(response)
        self.assertIn(b'Author parameter is empty', response.data, 'Author should not be empty')

    def test_add_onInvalidGroupId_fails(self):
        value = json.dumps({'name': 'Test name', 'content': 'Test content', 'author': 'Test author', 'group': 'Invalid group'})
        response = self.test_app.post('/add', data=value, headers=request_headers)

        self.assertIn(app.ID_INVALID_MESSAGE.encode('utf-8'), response.data)

    def test_add_onInexistantGroupId_fails(self):
        id = '572d6c260a061f0330b8045f'
        value = json.dumps({'name': 'Test name', 'content': 'Test content', 'author': 'Test author', 'group': id})
        response = self.test_app.post('/add', data=value, headers=request_headers)

        self.assertIn('Group with id \'{}\' does not exist'.format(id).encode('utf-8'), response.data)

    def add_notification(self):
        value = json.dumps({'name': 'Test name', 'content': 'Test content', 'author': 'Test author'})
        response = self.test_app.post('/add', data=value, headers=request_headers)
        print(response.data)
        id = str(json.loads(response.data.decode('utf-8'))['id'])
        return id

    def is_json(self, response_):
        self.assertEquals(response_.mimetype, 'application/json')

if __name__ == '__main__':
    unittest.main()
