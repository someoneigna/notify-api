from app.exceptions import *


def check_id(id_):
    if not id_ or len(id_) != 24 or not id_.isalnum():
        return False

    try:
        int(id_, 16)
    except ValueError:
        return False
    return True


def check_parameters(request_, parameters):
    for param in parameters:
        if param not in request_.json:
            raise ParameterMissingError('Missing \'{}\' parameter.'.format(param))
        if not request_.json[param]:
            raise ParameterEmptyError('{} parameter is empty.'.format(param.title()))
