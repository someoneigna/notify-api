from functools import partial
from functools import wraps
from flask import make_response, Request, jsonify, request


def requires_json_params(params = None, code=406):
    json_mediatype = 'application/json'
    msg = 'Call expects an \'application/json\' request'
    if params is not None:
        msg += ' with params ' + ' '.join(params)
    else:
        msg += '.'

    def decorated(function):
        @wraps(function)
        def wrapped():
            if json_mediatype not in request.headers['Content-Type']:
                return make_response(jsonify(message=msg), code)
            return function()
        return wrapped
    return decorated
