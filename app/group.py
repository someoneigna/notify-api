from flask import Flask, request, Response, make_response, jsonify, render_template, flash
from flask_mongoengine import MongoEngine
from app.auth import *
from app import db
from app.user_types import *


def clear_cache():
    pass


def register():
    return render_template('register.html')


def get_all():
    values = db.Group.objects.all()
    if values is None or len(values) == 0:
        return make_response(jsonify(message='Not data available'), 200)
    output = "["
    output += ','.join([v.to_json() for v in values]) + ']'
    return Response(response=output, status=200, mimetype='application/json')


def get(_id):	
    query_id = _id
    result = db.Group.objects(id=query_id)

    if len(result) == 0:
        return make_response(jsonify(message='Group not found.', id=_id), 404)
    output = result.get().to_json()
    return Response(response=output, status=200, mimetype='application/json')


def group_set(_id, _name, _active):
    query_id = _id
    result = db.Group.objects(id=query_id)
    if len(result) == 0:
        return make_response(jsonify(messsage='Group with id \'{}\' does not exist.'.format(_id)), 200)
    group = result.get()

    group.modify(name=_name, active=_active)

    clear_cache()

    return make_response(jsonify(message='Group successfully updated.', id=_id), 200)


def add(_name):	
    if len(db.Group.objects(name=_name)) > 0:
        return make_response(jsonify(messsage='Group with title \'{}\' already exists.'.format(_name)), 200)

    group = db.Group(name=_name)
    group.save()

    clear_cache()

    return make_response(jsonify(message='Group added correctly', id=str(group.id)), 201)


def remove(_id):
    query_name = _id
    group_matched = db.Group.objects(id=query_name)

    if len(group_matched) == 0:
        response = make_response(jsonify(message='Group not found \'{}\'.'.format(_id), id=_id), 404)
    else:
        group = group_matched.get()
        group.modify(active=False)
        clear_cache()
        response = make_response(jsonify(message='Group successfully deleted \'{}\'.'.format(_id), id=_id), 200)
    return response


def addadmin(user_id, group_id):

    resultuser = db.ClientUser.objects(id=user_id)
    if len(resultuser) == 0:
        return make_response(jsonify(messsage='User with id \'{}\' does not exist.'.format(user_id)), 200)
        
    resultgroup = db.Group.objects(id=group_id, active=True)    
    if len(resultgroup) == 0:
        return make_response(jsonify(messsage='Group with id \'{}\' does not exist.'.format(group_id)), 200)
        
    user = resultuser.get()
    usertype = get_usertype(user.user_type)

    if not usertype.permission.create_group:
        return make_response(jsonify(messsage='User with id \'{}\' is not group admin.'.format(group_id)), 200)
        
    group = resultgroup.get()
    if group in user.owned_groups:
        return make_response(jsonify(messsage='User \'{}\' is admin in group.'.format(group.name)), 200)

    user.owned_groups.append(group)
    user.modify(owned_groups = user.owned_groups)
    return make_response(jsonify(message='Group admin added to \'{}\'.'.format(group.name), id=user_id), 200)


def get_notifications(group_id):	
    values = db.Notification.objects(group=group_id)
    group = db.Group.objects(id=group_id)
    
    if len(group) == 0:
        return make_response(jsonify(message='Group with id \'{}\' does not exist.'.format(group_id)), 200)
        
    if len(values) == 0:
        output = "[]"
    else:    
        output = "["
        output += ','.join([v.to_json() for v in values]) + ']'
    return Response(response=output, status=200, mimetype='application/json')
