class ParameterMissingError(Exception):
    def __init__(self, message):
        self.message = message


class ParameterEmptyError(Exception):
    def __init__(self, message):
        self.message = message
