from flask_mongoengine import MongoEngine
from mongoengine import *
from app.user_types import *

import datetime
import os
import json

DATE_FORMAT = "%Y-%m-%d %I:%M:%S"


class Group(Document):
    name = StringField(max_lengh=120, required=True)
    active = BooleanField(default=True)

    def set(self, _name):
        self.name = _name

    def clean(self):
        """ Called before inserting in db """
        return True

    def to_json(self):
        d = {'id': str(self.id), 'name': self.name, 'active': self.active}
        return json.dumps(d)

def group_list_json(group_list):
    output = "["
    for i, g in enumerate(group_list):
        output += '{\"id\":\"' + str(g.id) + '\"' + ',\"name\": \"' + g.name + '\",\"active\":' + '\"' + str(g.active) + '\"' +'}'
        if i + 1 < len(group_list):
            output += ','
    return output + "]"
    
def values_tojson(keys, values):
    result = "{"
    for i,k in enumerate(keys):
        if values[i][0] != "[":
            value_str = '\"' + str(values[i]) + '\"'
        else:
            value_str = values[i]
        result += '\"' + str(k) + '\":' + value_str
        if i + 1 < len(keys):
            result += ','
    result += "}"
    return result
    
class ClientUser(Document): # Collection name is client_user
    username = StringField(max_length=120, required=True)
    password = StringField(max_length=120, required=True)
    email = StringField(max_length=120, required=True)
    user_type = StringField(max_length=120, required=True)
    owned_groups = ListField(ReferenceField(Group))
    subscription_groups = ListField(ReferenceField(Group))
    creationDate = DateTimeField(default=datetime.datetime.utcnow(), required=True)

    def set(self, _username, _password, _email, _type):
        self.username = _username
        self.password = _password
        self.email = _email
        self.user_type = _type

    def to_json(self):
        return values_tojson(["id", "username", "email", "usertype", "owned_groups", "subscription_groups", "creationDate"], [str(self.id), self.username, self.email, self.user_type, group_list_json(self.owned_groups), group_list_json(self.subscription_groups), self.creationDate.strftime(DATE_FORMAT)])


class User(Document):
    username = StringField(max_length=120, required=True)
    password = StringField(max_length=120, required=True)
    email = StringField(max_length=120, required=True)
    service_key = StringField(max_length=120, required=True)
    creation_date = DateTimeField(default=datetime.datetime.utcnow(), required=True)

    def clean(self):
        """Called before inserting in db"""
        return True


class Notification(Document):
    name = StringField(max_length=120, required=True)
    content = StringField(max_length=320, required=True)
    creation_date = DateTimeField(default=datetime.datetime.utcnow(), required=True)
    modification_date = DateTimeField(default=datetime.datetime.utcnow(), required=True)
    author = StringField(max_length=64, required=True)
    user = ReferenceField(User)
    tags = ListField(StringField(max_Length=32))
    meta = {'allow_inheritance': True}
    group = ReferenceField(Group)

    def set(self, _name, _content, _modification_date, _author):
        self.name = _name
        self.content = _content
        self.modification_date = _modification_date
        self.author = _author

    def clean(self):
        """Called before inserting in db"""
        return True

    def to_json(self):
        group_id_str = ""
        if self.group is not None:
            group_id_str = str(self.group.id)

        d = { 'id': str(self.id), 'name': self.name, 'content': self.content, 'creationDate': self.creation_date.strftime(DATE_FORMAT), 'modificationDate': self.modification_date.strftime(DATE_FORMAT), 'author': self.author, 'group_id': group_id_str }
        return json.dumps(d)


def init_db(mongo, mode):
    if mode == 'RELEASE':
        # Release
        mongo.connect('notifyutn', os.getenv('OPENSHIFT_MONGODB_DB_URL'))

    elif mode == 'TESTING':
        db = mongo.connect('mongonenginetest', 'mongomock://localhost')

    else:
        # Local test
        mongo.connect('notifyutn')
    print('MongoDB in {} mode'.format(mode))

    if Notification.objects.all().count() == 0:
        Notification(name='Ausencia del profesor', content='El dia Martes 13 de Octubre habra ausencia del profesor de Investigacion Operativa.', author='UTN').save()
        Notification(name='Aumento en la cuota', content='Habra un aumento del 15% entre los cuatrimestres.', author='UTN').save()
        Notification(name='Entrega de proyecto de Seminario', content='Recuerden que el día Viernes 16 de Octubre es la primer entrega fecha límite es el Jueves 22.', author='UTN').save()
