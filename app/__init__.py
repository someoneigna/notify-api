from flask import Flask, request, Response, make_response, jsonify, render_template, flash
from flask_mongoengine import MongoEngine
from flask_cache import Cache
from app.db import *
from app.auth import *
from app.email import *
from app.notification import *
from app.request_guards import *
from app.group import *
from app.user import *
from app.exceptions import *
import app.validation

# For service key generation in user registration
import uuid

settings = {'MODE': 'DEBUG'}
flask = Flask(__name__)
cache = Cache(flask, config={'CACHE_TYPE': 'simple'})

flask.config["MONGODB_SETTINGS"] = {'DB': "notifyutn"}
flask.config["SECRET_KEY"] = "eb401577-a87d-4240-a7c0-1efc0d58e443"

ID_INVALID_MESSAGE = json.dumps({'message': "Missing ID embedded parameter in URL or invalid ID."})


def init():
    if settings['MODE'] == 'TESTING':
        flask.config["MONGODB_SETTINGS"]['DB'] = 'testdb'
        flask.config['TESTING'] = True
        flask.config['TEMP_DB'] = True
    mongo = MongoEngine(flask)
    db.init_db(mongo, settings['MODE'])


def get_flask():
    return flask


@flask.route("/send_email", methods=['POST'])
def send_email():
    if request.form['username'] is None or\
     request.form['password'] is None or\
     request.form['email'] is None:
        return make_response(jsonify(message='Failed registration, parameters were incomplete.'), 402)

    target = request.form['email']
    user = request.form['username']
    pwd = request.form['password']

    if len(db.User.objects(username=user)) > 0 or\
       len(db.User.objects(email=target)) > 0:
        error = 'Failed registration, account already registered.'
        flash(error)
        return render_template('register.html', error=error)

    user_key = str(uuid.uuid4())
    User(username=user, password=pwd, service_key=user_key, email=target).save()

    text = 'Welcome to NotifyAPI, this are your account details.\nUsername:{}\nKey:{}\n\
\nUse it with care.\n NotifyAPI Team.'.format(user, user_key)
    subject = 'NotifyAPI access account registration'
    msg = MIMEText(text)
    msg['Subject'] = subject
    msg['To'] = target

    try:
        email.send_email(target, msg)
    except smtplib.SMTPConnectError as e:
        print('Error sending registration email:{}'.format(e))

    flash('Registration email was send to {}'.format(target))
    return render_template('index.html')


@flask.route("/register")
def register():
    return notification.register()


@flask.route("/set", methods=['POST'])
@requires_auth
@requires_json_params(['id', 'name',  'content', 'author'])
def set_notification():
    try:
        validation.check_parameters(request, ['id', 'name', 'content', 'author'])
        return notification.set_notification(request.json['id'], request.json['name'], request.json['content'], request.json['author'])
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@cache.cached(timeout=50)
@flask.route("/all", methods=['GET'])
@requires_auth
def get_all():
    return notification.get_all()


@flask.route("/get/<string:_id>", methods=['GET'])
@requires_auth
def get(_id):
    if not validation.check_id(_id):
        return make_response(ID_INVALID_MESSAGE, 400)
    return notification.get(_id)


@flask.route("/notification/group/<string:_id>", methods=['GET'])
@requires_auth
def get_notification_group(_id):
    if not validation.check_id(_id):
        return make_response(ID_INVALID_MESSAGE, 400)
    return group.get_notifications(_id)


@flask.route("/add", methods=['POST'])
@requires_auth
@requires_json_params(['name', 'content', 'author'])
def add():
    try:
        validation.check_parameters(request, ['name', 'content', 'author'])
        group_ = None
        if 'group' in request.json:
            group_ = request.json['group']
            if not validation.check_id(group_):
                return make_response(ID_INVALID_MESSAGE, 400)
        return notification.add(request.json['name'], request.json['content'], request.json['author'], group_)
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@flask.route('/remove/<string:_id>', methods=['DELETE'])
@requires_auth
def remove(_id):
    if not validation.check_id(_id):
        return make_response(ID_INVALID_MESSAGE, 400)
    return notification.remove(_id)


@flask.route("/group/set", methods=['POST'])
@requires_auth
@requires_json_params(['id', 'name', 'active'])
def group_set():
    try:
        validation.check_parameters(request, ['id', 'name', 'active'])
        return group.group_set(request.json['id'], request.json['name'], request.json['active'])
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@cache.cached(timeout=50)
@flask.route("/group/all", methods=['GET'])
@requires_auth
def group_get_all():
    return group.get_all()


@flask.route("/group/get/<string:_id>", methods=['GET'])
@requires_auth
def group_get(_id):
    if not validation.check_id(_id):
        return make_response(ID_INVALID_MESSAGE, 400)
    return group.get(_id)


@flask.route("/group/add", methods=['POST'])
@requires_auth
@requires_json_params(['name'], 400)
def group_add():
    try:
        validation.check_parameters(request, ['name'])
        return group.add(request.json['name'])
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@flask.route('/group/remove/<string:_id>', methods=['DELETE'])
@requires_auth
def group_remove(_id):
    if not validation.check_id(_id):
        return make_response(ID_INVALID_MESSAGE, 400)
    return group.remove(_id)


@flask.route("/group/addadmin", methods=['POST'])
@requires_auth
@requires_json_params(['user_id', 'group_id'])
def group_addadmin():
    try:
        validation.check_parameters(request, ['group_id', 'user_id'])
        return group.addadmin(request.json['user_id'], request.json['group_id'])
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@flask.route('/range', methods=['POST'])
@requires_auth
def notification_range():
    try:
        validation.check_parameters(request, ['start', 'end'])
        return notification.get_range(request.json['start'], request.json['end'])
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@flask.route("/user/set", methods=['POST'])
@requires_auth
@requires_json_params(['id', 'username', 'password', 'type', 'email'])
def user_set():
    try:
        validation.check_parameters(request, ['id', 'username', 'password', 'type', 'email'])
        return user.set_user(request.json['id'], request.json['username'], request.json['password'], request.json['type'], request.json['email'])
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@cache.cached(timeout=50)
@flask.route("/user/all", methods=['GET'])
@requires_auth
def user_get_all():
    return user.get_all()


@flask.route("/user/get/<string:_id>", methods=['GET'])
@requires_auth
def user_get(_id):
    if not validation.check_id(_id):
        return make_response(ID_INVALID_MESSAGE, 400)

    return user.get(_id)


@flask.route("/user/add", methods=['POST'])
@requires_auth
@requires_json_params(['username', 'password', 'usertype', 'email'])
def user_add():
    try:
        validation.check_parameters(request, ['username', 'password', 'usertype', 'email'])
        return user.add(request.json['username'], request.json['password'], request.json['usertype'], request.json['email'])
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@flask.route('/user/remove/<string:_id>', methods=['DELETE'])
@requires_auth
def user_remove(_id):
    if not validation.check_id(_id):
        return make_response(ID_INVALID_MESSAGE, 400)
    return user.remove(_id)


@flask.route("/user/subscription", methods=['POST'])
@requires_auth
@requires_json_params(['user_id, group_id'])
def user_subscription_add():
    try:
        validation.check_parameters(request, ['user_id', 'group_id'])
        if not validation.check_id(request.json['user_id']):
            return make_response(ID_INVALID_MESSAGE, 400)
        if not validation.check_id(request.json['group_id']):
            return make_response(ID_INVALID_MESSAGE, 400)
        return user.subscription(request.json['user_id'], request.json['group_id'])
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@flask.route("/user/subscriptions/remove", methods=['POST'])
@requires_auth
@requires_json_params(['user_id, group_id'])
def user_subscription_remove():
    try:
        validation.check_parameters(request, ['user_id', 'group_id'])
        if not validation.check_id(request.json['user_id']):
            return make_response(ID_INVALID_MESSAGE, 400)
        if not validation.check_id(request.json['group_id']):
            return make_response(ID_INVALID_MESSAGE, 400)
        return user.remove_subscription(request.json['user_id'], request.json['group_id'])

    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)

        
@flask.route("/user/login", methods=['POST'])
@requires_auth
@requires_json_params(['password', 'email'])
def user_login():
    try:
        validation.check_parameters(request, ['password', 'email'])
        return user.login(request.json['email'], request.json['password'])
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


@flask.route("/user/groups/<string:_id>", methods=['GET'])
@requires_auth
def user_groups(_id):
    try:
        if not validation.check_id(_id):
            return make_response(ID_INVALID_MESSAGE, 400)
        return user.groups(_id)
    except (ParameterMissingError, ParameterEmptyError) as e:
        return make_response(jsonify(message=e.message), 400)


if __name__ == '__main__':
    flask.run(threaded=True)
