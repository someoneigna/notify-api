from flask import Flask, request, Response, make_response, jsonify, render_template, flash
from app.auth import *
from app import db, Q
import datetime
import app.validation as validation
from app.exceptions import *


def clear_cache():
    pass


def register():
    """ Render register view """
    return render_template('register.html')


def get_all():
    values = db.Notification.objects(group=None)
    if values is None or len(values) == 0:
        return make_response(jsonify(message='Not data available'), 200)
    output = "["
    output += ','.join([v.to_json() for v in values]) + ']'
    return Response(response=output, status=200, mimetype='application/json')


def get(_id):   
    query_id = _id
    value = db.Notification.objects(id=query_id)

    if len(value) == 0:
        return make_response(jsonify(message='Notification not found.'.format(_id), id=_id), 404)
    result = value.get().to_json()
    return Response(response=result, status=200, mimetype='application/json')


def set_notification(_id, _name, _content, _author):

    query_id = _id
    result = db.Notification.objects(id=query_id)
    if len(result) == 0:
        return make_response(jsonify(messsage='Notification with id \'{}\' does not exist.'.format(_id)), 400)
    notification = result.get()

    notification.modify(name=_name, content=_content, author=_author, modification_date=datetime.datetime.utcnow())

    clear_cache()

    return make_response(jsonify(message='Notification successfully updated.', id=_id), 200)


def add(_name, _content, _author, _group):

    if _group is not None:
        group = db.Group.objects(id=_group, active=True)
        if len(group) == 0:
            return make_response(jsonify(messsage='Group with id \'{}\' does not exist.'.format(_group)), 400)
    
    _user = db.User.objects(service_key=request.authorization.username).get()
    if _user is None:
        return make_response(jsonify(message='Cant add notification, user is invalid.'), 400)
        
    notification_name = _name
    notification_content = _content
    notification = db.Notification(name=notification_name, content=notification_content, user=_user, author=_author, group=_group)
    notification.save()

    clear_cache()

    return make_response(jsonify(message='Notification added correctly', id=str(notification.id)), 201)


def remove(_id):
    query_name = _id
    notification_matched = db.Notification.objects(id=query_name)

    if len(notification_matched) == 0:
        response = make_response(jsonify(message='Notification not found.'.format(_id), id=_id), 404)
    else:
        for n in notification_matched:
            n.delete()
        clear_cache()
        response = make_response(jsonify(message='Notification successfully deleted.'.format(_id), id=_id), 200)
    return response


def get_range(_start, _end):
    start = datetime.datetime.strptime(_start, "%Y-%m-%d").date()
    end = datetime.datetime.strptime(_end, "%Y-%m-%d").date()

    if start > end:
        return make_response(jsonify(message='{} is before {} date.'.format(_start, _end)), 400)

    values = db.Notification.objects(Q(creation_date__gte=start) & Q(creation_date__lte=end))
    if values is None:
        return make_response(jsonify(message='No notifications found in specified date range.'), 200)

    output = "["
    output += ','.join([v.to_json() for v in values]) + ']'
    return Response(response=output, status=200, mimetype='application/json')


