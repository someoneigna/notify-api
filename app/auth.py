from functools import wraps
from flask import make_response, jsonify, request
from app import db

"""FIREBASE_URL = 'https://notifyRESTPI.firebaseio.com'

from firebase import firebase
firebase = firebase.FirebaseApplication(FIREBASE_URL, None)"""


def firebase():
    pass


def get_users():
    result = firebase.get('/user', None)
    print(result)
    return result


def get_user(user_id):
    result = firebase.get('/user', user_id)
    print(result)
    return result


def requires_auth(function):
    @wraps(function)
    def decorated(*args, **kwargs):
        try:
            if request.authorization.username is None or\
               request.authorization.password is None:
                return missing_credentials()
        except AttributeError:
            return missing_credentials()

        service_key = request.authorization.username
        users = db.User.objects(service_key=service_key)

        if len(users) < 1 or request.authorization.password != users[0].password:
            return make_response(jsonify(message='Error, user not registered.'), 401)
        else:
            return function(*args, **kwargs)
    return decorated


def missing_credentials():
    return make_response(jsonify(message='User didn\'t provide credentials'), 401)
