from flask import Flask, request, Response, make_response, jsonify, render_template, flash
from flask_mongoengine import MongoEngine
from app import db
from app.auth import *
from app.user_types import *


def clear_cache():
    pass


def get_all():
    values = db.ClientUser.objects.all()
    if  values is None or len(values) == 0:
        return make_response(jsonify(message='Not data available'), 200)
    output = "["
    output += ','.join([v.to_json() for v in values]) + ']'
    return Response(response=output, status=200, mimetype='application/json')


def get(_id):   
    query_id = _id
    value = db.ClientUser.objects(id=query_id)

    if len(value) == 0:
        return make_response(jsonify(message='User not found.'.format(_id), id=_id), 404)
    result = value.get().to_json()
    return Response(response=result, status=200, mimetype='application/json')


def set_user(_id, _username, _password, _type, _email):
    query_id = _id
    result = db.ClientUser.objects(id=query_id)
    
    if len(result) == 0:
        return make_response(jsonify(messsage='User with id \'{}\' does not exist.'.format(_id)), 200)
    user = result.get()
    usertype = get_usertype(_type)
    user.modify(username=_username, password=_password, user_type=usertype.name, email=_email)

    clear_cache()

    return make_response(jsonify(message='User succesfuly updated.', id=_id), 200)


def add(_username, _password, _type, _email):   
    type = get_usertype(_type)
    if type is None:
        return make_response(jsonify(messsage='Invalid user type. \'{}\' '.format(_type)), 200)

    if len(db.ClientUser.objects(username=_username)) > 0:
        return make_response(jsonify(messsage='User with username \'{}\' already exists.'.format(_username)), 200)

    user = db.ClientUser(username=_username, password=_password, user_type=type.name, email=_email)
    user.save()

    clear_cache()

    return make_response(jsonify(message='User added correctly', id=str(user.id)), 201)


def remove(_id):
    query_name = _id
    user_matched = db.ClientUser.objects(id=query_name)

    if len(user_matched) == 0:
        response = make_response(jsonify(message='User not found.'.format(_id), id=_id), 404)
    else:
        for n in user_matched:
            n.delete()
        clear_cache()
        response = make_response(jsonify(message='User successfully deleted.'.format(_id), id=_id), 200)
    return response
   
   
def subscription(user_id, group_id):

    resultuser = db.ClientUser.objects(id=user_id)
    if len(resultuser) == 0:
        return make_response(jsonify(messsage='User with id \'{}\' does not exist.'.format(user_id)), 200)
        
    resultgroup = db.Group.objects(id=group_id, active=True)    
    if len(resultgroup) == 0:
        return make_response(jsonify(messsage='Group with id \'{}\' does not exist.'.format(group_id)), 200)
        
    user = resultuser.get()
    
    group = resultgroup.get()
    if group in user.subscription_groups:
        return make_response(jsonify(messsage='User is already in group \'{}\'.'.format(group.name)), 200)
        
    user.subscription_groups.append(group)
    user.modify(subscription_groups = user.subscription_groups)
    return make_response(jsonify(message='User registered in the group.', id=user_id), 200)


def remove_subscription(user_id, group_id):

    user = db.ClientUser.objects(id=user_id)
    if len(user) == 0:
        return make_response(jsonify(messsage='User with id \'{}\' does not exist.'.format(user_id)), 400)

    group = db.Group.objects(id=group_id, active=True)
    if len(group) == 0:
        return make_response(jsonify(messsage='Group with id \'{}\' does not exist.'.format(group_id)), 400)
        
    user_value = user.get()
    
    group_value = group.get()
    if group_value in user_value.subscription_groups:
        user_value.subscription_groups.remove(group_value)
        user_value.modify(subscription_groups=user_value.subscription_groups)
    else:
        return make_response(jsonify(messsage='User is not in group \'{}\'.'.format(group_id)), 400)
        
    return make_response(jsonify(message='User successfully removed from group.', id=user_id), 200)


def login(_email, _password):

    user = db.ClientUser.objects(email=_email)
    if len(user) == 0:
        return make_response(jsonify(messsage='Invalid login data \'{}\'.'.format(_email)), 400)
    
    user_value = user.get()
    if user_value.password == _password:
        return make_response(jsonify(message='Successful login.', id=str(user_value.id), username=user_value.username, usertype=str(user_value.user_type)), 200)
    else:
        return make_response(jsonify(messsage='Failed to login \'{}\'.'.format(_email)), 400)


def groups(_id):
    values = db.ClientUser.objects(id=_id)

    if values is None or len(values) == 0:
        return make_response(jsonify(message='User with id \'{}\' does not exist.'.format(_id)), 400)

    user = values[0]
    owned_groups = '[' + ','.join([v.to_json() for v in user.owned_groups]) + ']'
    subscribed_groups = '[' + ','.join([v.to_json() for v in user.subscription_groups]) + ']'

    output = '{' + '"owned_groups":' + owned_groups + ',' + '"subscribed_groups":' + subscribed_groups + '}'
    return Response(response=output, status=200, mimetype='application/json')
