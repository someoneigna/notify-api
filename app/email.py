import smtplib
from email.mime.text import MIMEText

API_EMAIL = 'alzadacorpnotifyapi@gmail.com'


class EmailConfig:
    def __init__(self, server, port, auth):
        self.server = server
        self.port = port
        self.username = auth['username']
        self.password = auth['password']

DEFAULT_CONFIG =  EmailConfig('smtp.gmail.com', 465, {'username': 'alzadacorpnotifyapi', 'password': 'n0t1fyutn'})


def send_email(target, msg):
    server = smtplib.SMTP_SSL(DEFAULT_CONFIG.server, DEFAULT_CONFIG.port)
    server.ehlo()
    server.login(DEFAULT_CONFIG.username, DEFAULT_CONFIG.password)
    server.sendmail(API_EMAIL, target, msg.as_string())
    server.close()
