class Permissions:
    create_notification = False
    create_group = False
    create_user = False

SubscriptorPermission = Permissions()

GroupAdminPermission = Permissions()
GroupAdminPermission.create_group = True
GroupAdminPermission.create_notification = True

AdminPermission = Permissions()
AdminPermission.create_notification = True
AdminPermission.create_group = True
AdminPermission.create_user = True


class UserType:
    def __init__(self, name, permission):
        self.name = name
        self.permission = permission

Admin = UserType("admin", AdminPermission)
Subscriptor = UserType("subscriptor", SubscriptorPermission)
GroupAdmin = UserType("group_admin", GroupAdminPermission)


def get_usertype(value):
    value = value.lower()
    if value == Admin.name: return Admin
    if value == Subscriptor.name: return Subscriptor
    if value == GroupAdmin.name: return GroupAdmin
    else: return None

