from flask_script import Manager, Server
from flask import url_for
import app

import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

manager = Manager(app.get_flask())

# Uncomment to enable SSL
"""context = ('server.crt', 'server.key')"""

# Turn on debugger by default and reloader
manager.add_command("runserver", Server(
    use_debugger=True,
    use_reloader=True,
    # ssl_context=context,
    host='127.0.0.1')
)

manager.add_command("runserver-LAN", Server(
    use_debugger=False,
    use_reloader=True,
    # ssl_context=context,
    host='192.0.0.0')
)

manager.add_command("runserver-release", Server(
    use_debugger=False,
    use_reloader=False,
    host='0.0.0.0')
)


@manager.command
def list_routes():
    import urllib
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = urllib.parse.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
        output.append(line)
    
    for line in sorted(output):
        print(line)

if __name__ == "__main__":
    app.init()
    manager.run()
